import Vue from 'vue'
import { CheckCircleIcon, XIcon } from 'vue-feather-icons'

Vue.component('CheckCircleIcon', CheckCircleIcon)
Vue.component('XIcon', XIcon)
