import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    active_theme: 'light',
    active_filter: 'all'
  },
  mutations: {
    setTheme(state, theme) {
      let first_color = '#ffffff'
      let second_color = '#2E3E52'
      let third_color = '#67C23A'
      let fourth_color = '#46a0f7'

      state.active_theme = theme

      if (theme === 'light') {
        document.documentElement.style
            .setProperty('--main-color', first_color)
        document.documentElement.style
            .setProperty('--text-color', second_color)
        document.documentElement.style
            .setProperty('--title-color', fourth_color)
      } else {
        document.documentElement.style
            .setProperty('--main-color', second_color)
        document.documentElement.style
            .setProperty('--text-color', first_color)
        document.documentElement.style
            .setProperty('--title-color', third_color)
      }
    },
    setActiveFilter(state, filter) {
      state.active_filter = filter
    }
  },
  actions: {
  },
  modules: {
  }
})
